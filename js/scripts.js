$(function() {
    let letras = /^[a-z\d](?:[a-z\d]| (?! |$)){0,29}$/i;
    
    $("#textNome").on("input", function() {
        if (letras.test($(this).val())) {
            // Se for válido, remove a classe inválido
            $(this).removeClass("invalido");
        } else {
            // Se for inválido, inclui a classe de inválido
            $(this).addClass("invalido");
        }
    });
});