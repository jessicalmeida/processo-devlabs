<!DOCTYPE html>
<html lang="pt-br">	
	<head>	  
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Bootstrap CSS -->	 
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/bootstrap.css" rel="stylesheet">
		<script src="../js/jquery.js"> </script>
  		<script src="../js/bootstrap.min.js"></script>
    </head>	
	<body>
        <?php
            //Conexao com o banco
            include("../banco/conecta.php");
            
            // Dados do formulario ja validados
            $nome = $_POST['textNome'];
            $email = $_POST['inputEmail'];
            $senha = $_POST['inputSenha'];
            
            //Inserindo os dados no banco - tabela1
            $insert = $conn->prepare(
                'INSERT INTO tabela1 (id, nome, email, senha, ultima_alteracao) VALUES (NULL, :nome, :email, :senha, now())'
            );
             
            $insert->bindValue(':nome', $nome);
            $insert->bindValue(':email', $email);
            $insert->bindValue(':senha', md5($senha));
            
            $executa = $insert->execute();
            
            if($executa)
            {
                echo "<h4> Dados inseridos! </h4>";
                echo "<br>";
                echo "<a href='../index.html'> <h4> Voltar </h4> </a>";
                
            }
            else
            {
               echo "<h4> Erro ao inserir os dados. </h4>";
            }
        
        ?>
    </body>
</html>